import React, { useState } from 'react';
import './RenderCard.scss';

import RenderModalDelete from '../RenderModalDelete/RenderModalDelete.jsx';
import FavoriteIcon from '../svg/FavoriteIcon/FavoriteIcon.jsx';
import RenderModalFavorite from '../RenderModalFavorite/RenderModalFavorite.jsx';

const RenderCard = ({ products, onFavoriteClick, favorites, cart, onCartClick }) => {
  const [isModalDeleteOpen, setIsModalDeleteOpen] = useState(false);
  const [isModalFavoriteOpen, setIsModalFavoriteOpen] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [addToCartConfirmed, setAddToCartConfirmed] = useState(false);

  const toggleModalDelete = (product) => {
    setSelectedProduct(product);
    setIsModalDeleteOpen(!isModalDeleteOpen);
    setAddToCartConfirmed(false);
  };

  const toggleModalFavorite = (product) => {
    setSelectedProduct(product);
    setIsModalFavoriteOpen(!isModalFavoriteOpen);
  };

  const handleAddToCart = () => {
    if (addToCartConfirmed) {
      onCartClick(selectedProduct.id);
    }
    setIsModalDeleteOpen(false);
  };

  return (
    <div>
      {products.map((product) => (
        <div key={product.id} className="card">
          <div className="imageContainer">
            <img src={product.image} alt="Fruit image" className="image"/>
            <FavoriteIcon
              className="cardIcon"
              isFavorite={favorites.includes(product.id)}
              onClick={() => {
                onFavoriteClick(product.id);
                toggleModalFavorite(product);
              }}
            />
          </div>
          <div className="cardInfo">
            <h2>{product.productName}</h2>
            <div className="cardDescriptionContainer">
              <div className="cardDescription">
                <p>Color: {product.color}</p>
                <p>Price: {product.price} grn</p>
              </div>
              <div className="cardButtonContainer">
                <p className="cardArticle">Article: {product.article}</p>
                <button
                  className="cardButton secondaryButton"
                  onClick={() => {
                    toggleModalDelete(product);
                  }}
                >
                  Add to cart
                </button>
              </div>
            </div>
          </div>
        </div>
      ))}
      {isModalDeleteOpen && (
        <RenderModalDelete
          setShowModalDelete={setIsModalDeleteOpen}
          src={selectedProduct.image_path}
          onCartClick={onCartClick}
          product={selectedProduct}
          setAddToCartConfirmed={setAddToCartConfirmed}
        />
      )}
      {isModalFavoriteOpen && (
        <RenderModalFavorite
          setShowModalFavorite={setIsModalFavoriteOpen}
          onFavoriteClick={onFavoriteClick}
          product={selectedProduct}
          favorites={favorites}
        />
      )}
    </div>
  );
};

export default RenderCard;
