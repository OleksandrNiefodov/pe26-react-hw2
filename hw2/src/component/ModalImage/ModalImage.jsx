import React from 'react';
import PropTypes from 'prop-types';
import './ModalImage.sass';

const ModalImage = ({ src, alt }) => (
  <div className="modal-image">
    <img src={src} />
  </div>
);

ModalImage.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
};

export default ModalImage;