import React from 'react';
import PropTypes from 'prop-types';
import './Header.scss';
import NavMenu from '../NavMenu/NavMenu.jsx';
import ButtonContainer from '../ButtonContainer/ButtonContainer.jsx';

const Header = ({ favorites, cart }) => {

  return (
    <div className="header">
      <div className='header__nav-menu'><NavMenu />
      </div>
      
      <ButtonContainer favorites={favorites} cart={cart}/>
    </div>
  );
};

Header.propTypes = {
  favorites: PropTypes.array.isRequired,
  cart: PropTypes.array.isRequired,
  handleFavoriteClick: PropTypes.func.isRequired,
};

export default Header;