import React from 'react';
import PropTypes from 'prop-types';
import './ModalBody.sass';

const ModalBody = ({ children }) => (
  <div className="modal-body">
    {children}
  </div>
);

ModalBody.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ModalBody;