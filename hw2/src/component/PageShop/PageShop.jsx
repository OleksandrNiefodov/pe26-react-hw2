import React, { useState, useEffect } from 'react';
import axios from 'axios';
import RenderCard from '../RenderCard/RenderCard'
import "./ShopPage.scss";

const ShopPage = ({ onFavoriteClick, favorites, cart, onCartClick, showModalDelete, toggleModalDelete  }) => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('database.json');
        setProducts(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);

  return (
    <div className='cards-wrapper'>
    
      <h1 className='cards-container__title'>Fruits Store</h1>
      
      <div className='cards-container'><RenderCard 
        products={products} 
        onFavoriteClick={onFavoriteClick} 
        favorites={favorites}
        onCartClick={onCartClick} cart={cart}
      /></div>
      
    </div>
  );
};

export default ShopPage;
