import React from 'react';
import PropTypes from 'prop-types';
import './NavMenu.scss';


const NavMenu = () => {
  return (
    <ul className="NavMenu">
      <li><p>Home</p></li>
      <li><p>Shop</p></li>
    </ul>
  );
};

export default NavMenu;