import React from 'react';
import PropTypes from 'prop-types';

const ModalClose = ({ onClick }) => (
  <button className="modal-close" onClick={onClick}>×</button>
);

ModalClose.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default ModalClose;