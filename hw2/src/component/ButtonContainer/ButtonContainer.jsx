import './ButtonContainer.scss';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import CartIcon from '../svg/CartSVG/CartIcon.jsx';
import FavoriteIcon from '../svg/FavoriteIcon/FavoriteIcon.jsx';

const ButtonContainer = ({ favorites, cart }) => {

  useEffect(() => {

  }, [favorites])

  useEffect(() => {

  }, [cart])

  const productId = 'some-product-id';

  return (
    <div className="ButtonContainer">
      <button className="Favorite" >
        <FavoriteIcon isStatic />
        <span className='favoriteCount'>{favorites.length}</span>
      </button>
      <button className="Cart">
        <CartIcon />
        <span className='cartCount'>{cart.length}</span>
      </button>

    </div>
  );
};

ButtonContainer.propTypes = {
  favorites: PropTypes.array.isRequired,
  cart: PropTypes.array.isRequired,
};

export default ButtonContainer;
